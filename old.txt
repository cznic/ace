goos: linux
goarch: amd64
pkg: modernc.org/ace
cpu: AMD Ryzen 9 3900X 12-Core Processor            
BenchmarkEncodeEnwik6-24    	       5	 252803946 ns/op	   3.96 MB/s	13112216 B/op	    7659 allocs/op
BenchmarkEncodeEnwik6-24    	       5	 233008863 ns/op	   4.29 MB/s	13104366 B/op	    7588 allocs/op
BenchmarkEncodeEnwik6-24    	       5	 224565360 ns/op	   4.45 MB/s	13107147 B/op	    7615 allocs/op
BenchmarkEncodeEnwik6-24    	       5	 221586540 ns/op	   4.51 MB/s	13110620 B/op	    7645 allocs/op
BenchmarkEncodeEnwik6-24    	       5	 211730157 ns/op	   4.72 MB/s	13109836 B/op	    7639 allocs/op
BenchmarkEncodeEnwik6-24    	       5	 229822901 ns/op	   4.35 MB/s	13108185 B/op	    7623 allocs/op
BenchmarkEncodeEnwik6-24    	       5	 254022652 ns/op	   3.94 MB/s	13113769 B/op	    7675 allocs/op
BenchmarkEncodeEnwik6-24    	       4	 251879570 ns/op	   3.97 MB/s	13110968 B/op	    7647 allocs/op
BenchmarkEncodeEnwik6-24    	       4	 260182003 ns/op	   3.84 MB/s	13108284 B/op	    7622 allocs/op
BenchmarkEncodeEnwik6-24    	       5	 240360241 ns/op	   4.16 MB/s	13109888 B/op	    7637 allocs/op
BenchmarkDecodeEnwik6-24    	      42	  53711543 ns/op	  18.62 MB/s	15879817 B/op	    7566 allocs/op
BenchmarkDecodeEnwik6-24    	      24	  54383536 ns/op	  18.39 MB/s	15879250 B/op	    7558 allocs/op
BenchmarkDecodeEnwik6-24    	      43	  53087683 ns/op	  18.84 MB/s	15880005 B/op	    7569 allocs/op
BenchmarkDecodeEnwik6-24    	      37	  53361290 ns/op	  18.74 MB/s	15879836 B/op	    7567 allocs/op
BenchmarkDecodeEnwik6-24    	      25	  53853150 ns/op	  18.57 MB/s	15878677 B/op	    7552 allocs/op
BenchmarkDecodeEnwik6-24    	      46	  53191370 ns/op	  18.80 MB/s	15881113 B/op	    7583 allocs/op
BenchmarkDecodeEnwik6-24    	      30	  53758755 ns/op	  18.60 MB/s	15879925 B/op	    7568 allocs/op
BenchmarkDecodeEnwik6-24    	      30	  53054256 ns/op	  18.85 MB/s	15880244 B/op	    7573 allocs/op
BenchmarkDecodeEnwik6-24    	      26	  53857664 ns/op	  18.57 MB/s	15881015 B/op	    7581 allocs/op
BenchmarkDecodeEnwik6-24    	      24	  51959958 ns/op	  19.25 MB/s	15877692 B/op	    7540 allocs/op
PASS
ok  	modernc.org/ace	48.285s
