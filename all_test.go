// Copyright 2024 Jan Mercl. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package ace // import "modernc.org/ace"

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"time"

	"github.com/dustin/go-humanize"
)

const (
	window    = 32
	firstMeta = 256
)

func TestMain(m *testing.M) {
	flag.Parse()
	os.Exit(m.Run())
}

type reader struct {
	*bufio.Reader
	cnt int
}

func (r *reader) read() (Symbol, error) {
	b, err := r.ReadByte()
	if err == nil {
		r.cnt++
	}
	return Symbol(b), err
}

func TestMemory0(t *testing.T) {
	for _, v := range []string{
		"abcabdabcabd",
		"enwik3",
		"enwik4",
		"enwik5",
		"enwik6",
		"gettysburg",
		"gettysburgx10",
		"gettysburgx100",
		"zero3",
		"zero4",
		"zero5",
		"zero6",
	} {
		in := filepath.Join("testdata", v)
		f, err := os.Open(in)
		if err != nil {
			t.Fatalf("%v: %v", in, err)
		}

		r := &reader{Reader: bufio.NewReader(f)}
		t0 := time.Now()
		encoded := testEncode(t, window, firstMeta, r)
		te := time.Since(t0)
		f.Close()
		var maxUsed uint32
		for _, v := range encoded {
			maxUsed = max(uint32(v), maxUsed)
		}
		t0 = time.Now()
		decoded := testDecode(t, firstMeta, encoded)
		td := time.Since(t0)
		t.Logf(
			"%-15s: syms in=%9v syms out=%7v k=%5.2f%% te=%15v td=%15v",
			v, h(r.cnt), h(len(encoded)), float64(len(encoded))/float64(r.cnt)*100, te, td,
		)
		b, err := os.ReadFile(in)
		if err != nil {
			t.Fatal(err)
		}

		if !bytes.Equal(b, decoded) {
			t.Errorf("%v: len decoded=%v corrupted", in, h(len(decoded)))
			continue
		}

	}
}

func (s Symbol) String() string {
	if s < firstMeta {
		return strconv.QuoteRuneToASCII(rune(s))
	}

	return fmt.Sprintf("(%d)", s-firstMeta)
}

func (p pair) String() string {
	return fmt.Sprintf("{%s %s}", p.a, p.b)
}

func h(v interface{}) string {
	switch x := v.(type) {
	case int:
		return humanize.Comma(int64(x))
	case int32:
		return humanize.Comma(int64(x))
	case int64:
		return humanize.Comma(x)
	case uint32:
		return humanize.Comma(int64(x))
	case uint64:
		if x <= math.MaxInt64 {
			return humanize.Comma(int64(x))
		}
	}
	return fmt.Sprint(v)
}

type symbolReader interface {
	read() (Symbol, error)
}

func testEncode(t testing.TB, window int, firstMeta Symbol, in symbolReader) (r []Symbol) {
	e, err := NewEncoder(window, firstMeta, func(s Symbol) { r = append(r, s) })
	if err != nil {
		t.Fatal(err)
	}

	for {
		s, err := in.read()
		if err != nil {
			if err == io.EOF {
				break
			}

			t.Fatal(err)
		}

		e.Encode(s)
	}
	e.Flush()
	return r
}

func testDecode(t testing.TB, firstMeta Symbol, in []Symbol) (r []byte) {
	d, err := NewDecoder(firstMeta, func(s Symbol) { r = append(r, byte(s)) })
	if err != nil {
		t.Fatal(err)
	}

	for _, v := range in {
		d.Decode(v)
	}
	return r
}

func BenchmarkEncodeEnwik6(b *testing.B) {
	in := filepath.Join("testdata", "enwik6")
	f, err := os.Open(in)
	if err != nil {
		b.Fatalf("%v: %v", in, err)
	}

	defer f.Close()

	r := &reader{Reader: bufio.NewReader(f)}
	b.ReportAllocs()
	b.ResetTimer()
	b.SetBytes(1e6)
	for i := 0; i < b.N; i++ {
		if _, err := f.Seek(0, io.SeekStart); err != nil {
			b.Fatal(err)
		}

		r.Reader.Reset(f)
		testEncode(b, window, firstMeta, r)
	}
}

func BenchmarkDecodeEnwik6(b *testing.B) {
	in := filepath.Join("testdata", "enwik6")
	f, err := os.Open(in)
	if err != nil {
		b.Fatalf("%v: %v", in, err)
	}

	defer f.Close()

	enc := testEncode(b, window, firstMeta, &reader{Reader: bufio.NewReader(f)})

	b.ReportAllocs()
	b.ResetTimer()
	b.SetBytes(1e6)
	for i := 0; i < b.N; i++ {
		testDecode(b, firstMeta, enc)
	}
}
