// Copyright 2024 Jan Mercl. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main // import "modernc.org/ace/ace"

import (
	"bytes"
	"flag"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/dustin/go-humanize"
	"modernc.org/plot"
)

var (
	oGraph = flag.Bool("graph", false, "generate docs graph")
)

func TestMain(m *testing.M) {
	flag.Parse()
	os.Exit(m.Run())
}

func TestBitSum(t *testing.T) {
	const N = 1e6
	var sum, x uint64
	for n := uint32(0); n < N; n++ {
		if n > 1 {
			x = bitSum(uint64(n))
			if g, e := x, sum; g != e {
				t.Fatalf("n=%8d x=%8d sum=%8d", n, x, sum)
			}
			sum += uint64(bitWidth(n))
			continue
		}

		sum += 8
	}
	for n := uint64(1e3); n <= 1e9; n *= 10 {
		sum := bitSum(n)
		t.Logf("bitSum(%13v)=%14vb sz=%13vB", h(n), h(sum), h((sum+7)/8))
	}
}

func h(v interface{}) string {
	switch x := v.(type) {
	case int:
		return humanize.Comma(int64(x))
	case int32:
		return humanize.Comma(int64(x))
	case int64:
		return humanize.Comma(x)
	case uint32:
		return humanize.Comma(int64(x))
	case uint64:
		if x <= math.MaxInt64 {
			return humanize.Comma(int64(x))
		}
	}
	return fmt.Sprint(v)
}

func TestRoundTrip(t *testing.T) {
	temp := t.TempDir()
	for _, v := range []string{
		"abcabdabcabd",
		"enwik3",
		"enwik4",
		"enwik5",
		"enwik6",
		"enwik7",
		"enwik8",
		"enwik9",
		"gettysburg",
		"gettysburgx10",
		"gettysburgx100",
		"zero3",
		"zero4",
		"zero5",
		"zero6",
	} {
		func() {
			ifn := filepath.Join("..", "testdata", v)
			if _, err := os.Stat(ifn); err != nil {
				return
			}

			enc := filepath.Join(temp, v+".ace")
			t0 := time.Now()
			in, out, err := encode(ifn, enc)
			if err != nil {
				t.Fatal(err)
			}

			te := time.Since(t0)
			t0 = time.Now()
			dec := filepath.Join(temp, v)
			_, out2, err := decode(enc, dec)
			if err != nil {
				t.Fatal(err)
			}

			if out2 != in {
				t.Fatalf("in=%v out2=%v", h(in), h(out2))
			}

			td := time.Since(t0)
			t.Logf(
				"%-15s: bytes in=%13v bytes out=%13v k=%5.2f%% te=%15v td=%15v",
				v, h(in), h(out), float64(out)/float64(in)*100, te, td,
			)

			e, err := os.ReadFile(ifn)
			if err != nil {
				t.Fatal(err)
			}

			g, err := os.ReadFile(dec)
			if err != nil {
				t.Fatal(err)
			}

			if !bytes.Equal(g, e) {
				t.Errorf("%v: corrupted", ifn)
			}
		}()

	}
}

func TestGraph(t *testing.T) {
	if !*oGraph {
		t.Skip("enable with -graph")
	}

	const src = `
set term dumb size 135,40
set format y "%g %%"
set grid
$data << EOD
	x src    ace  gzip brotli
        3 enwik3 43.2 34.3 29.5
	4 enwik4 49.8 37.3 29.4
	5 enwik5 44.7 36.1 29.4
	6 enwik6 40.7 35.6 28.1
	7 enwik7 38.6 36.9 27.0
	8 enwik8 35.5 36.5 25.7
	9 enwik9 29.1 32.4 22.3
EOD
plot for [i=3:5] $data using 1:i:xticlabels(2) title columnhead with lines
`
	out, err := plot.Script([]byte(src))
	if err != nil {
		t.Fatal(err)
	}

	fmt.Printf("%s\n", out)
}
