// Copyright 2024 Jan Mercl. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Command ace demonstrates a concrete implementation of the ACE algorithm for
// byte sequences.
//
// # Overview
//
//   - The tool is intended for experimenting and exploring, not for production.
//   - Its performance has not been optimized.
//   - The file format is inefficient. No Huffman/arithmetic coding, no content
//     modeling etc.
//
// # Security considerations
//
// It is easy to create an ACE-bomb using the same principle as in the [Zip bomb].
//
// # Installation
//
//	$ go install modernc.org/ace/ace@latest
//
// # Usage
//
//	ace [-flags] path1 [path2]
//
// The flags are
//
//	-w
//		Enables overwriting the output file. (default false)
//
// If the command is invoked with one path then
//   - If path1 has suffix .ace the file is decompressed into a file without the suffix.
//   - Otherwise path1 is compressed into a file with the .ace suffix added.
//
// If the command is invoked with two paths then
//   - If path1 has suffix .ace then it is decompressed into path2.
//   - If path2 has sufix .ace then path1 is compressed into path2.
//
// # Examples
//
// Compress foo into foo.ace
//
//	$ ace foo
//
// Decompress foo.ace into foo
//
//	$ ace foo.ace
//
// Compress foo into bar.ace
//
//	$ ace foo bar.ace
//
// Decompress bar.ace into foo
//
//	$ ace bar.ace foo
//
// # Samples
//
//	=== RUN   TestRoundTrip
//	    all_test.go:109: abcabdabcabd   : bytes in=           12 bytes out=           10 k=83.33% te=         36.2µs td=        38.51µs
//	    all_test.go:109: enwik3         : bytes in=        1,000 bytes out=          432 k=43.20% te=        322.2µs td=        67.62µs
//	    all_test.go:109: enwik4         : bytes in=       10,000 bytes out=        4,978 k=49.78% te=     3.157548ms td=       420.29µs
//	    all_test.go:109: enwik5         : bytes in=      100,000 bytes out=       44,749 k=44.75% te=     24.44593ms td=     4.734127ms
//	    all_test.go:109: enwik6         : bytes in=    1,000,000 bytes out=      406,561 k=40.66% te=   218.019833ms td=    48.419003ms
//	    all_test.go:109: enwik7         : bytes in=   10,000,000 bytes out=    3,860,431 k=38.60% te=   2.630216717s td=   531.453043ms
//	    all_test.go:109: enwik8         : bytes in=  100,000,000 bytes out=   35,477,444 k=35.48% te=  28.991318603s td=   6.382845228s
//	    all_test.go:109: enwik9         : bytes in=1,000,000,000 bytes out=  291,087,860 k=29.11% te= 5m5.751635121s td=1m15.112020041s
//	    all_test.go:109: gettysburg     : bytes in=        1,463 bytes out=          891 k=60.90% te=      677.359µs td=       105.77µs
//	    all_test.go:109: gettysburgx10  : bytes in=       14,630 bytes out=        1,932 k=13.21% te=     1.922068ms td=      647.689µs
//	    all_test.go:109: gettysburgx100 : bytes in=      146,300 bytes out=        1,950 k= 1.33% te=      9.58534ms td=     6.132015ms
//	    all_test.go:109: zero3          : bytes in=        1,000 bytes out=           26 k= 2.60% te=        98.25µs td=        53.63µs
//	    all_test.go:109: zero4          : bytes in=       10,000 bytes out=           34 k= 0.34% te=       519.71µs td=      222.599µs
//	    all_test.go:109: zero5          : bytes in=      100,000 bytes out=           42 k= 0.04% te=     5.172695ms td=     1.972528ms
//	    all_test.go:109: zero6          : bytes in=    1,000,000 bytes out=           50 k= 0.01% te=    49.813767ms td=     19.51589ms
//	--- PASS: TestRoundTrip (420.77s)
//
// # File size vs Compression
//
//	50 % +----------------------------------------------------------------------------------------------------------------------------+
//	     |                 ** + ****               +                    +                   +                    +                    |
//	     |              ***   :     ***            :                    :                   :                    :        ace ******* |
//	     |           ***      :        ****        :                    :                   :                    :       gzip ####### |
//	     |         **         :            ***     :                    :                   :                    :     brotli $$$$$$$ |
//	     |      ***           :               **** :                    :                   :                    :                    |
//	45 % |-+.***..............:...................****..................:...................:....................:..................+-|
//	     | **                 :                    :  ****              :                   :                    :                    |
//	     |*                   :                    :      ****          :                   :                    :                    |
//	     |                    :                    :          ****      :                   :                    :                    |
//	     |                    :                    :              ****  :                   :                    :                    |
//	     |                    :                    :                  ******                :                    :                    |
//	40 % |-+..................:....................:....................:...******..........:....................:..................+-|
//	     |                    :                    :                    :         *******   :                    :                    |
//	     |                    :                    :                    :                *******                 :                    |
//	     |                  ########               :                    :                   :   *******          :                    |
//	     |             #####  :     ##########     :                    :         ##################################                  |
//	     |       ######       :               ####################################          :                 ***** ####              |
//	35 % |-+#####.............:....................:....................:...................:....................:.**...####........+-|
//	     |##                  :                    :                    :                   :                    :   ***    ####      |
//	     |                    :                    :                    :                   :                    :      ***     ####  |
//	     |                    :                    :                    :                   :                    :         **       ##|
//	     |                    :                    :                    :                   :                    :           ***      |
//	     |                    :                    :                    :                   :                    :              ***   |
//	30 % |-+..................:....................:....................:...................:....................:.................**-|
//	     |$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$          :                   :                    :                   *|
//	     |                    :                    :          $$$$$$$$$$$$$$$               :                    :                    |
//	     |                    :                    :                    :    $$$$$$$$$$     :                    :                    |
//	     |                    :                    :                    :              $$$$$$$$$$$$$$$$          :                    |
//	     |                    :                    :                    :                   :          $$$$$$$$$$$$$                  |
//	25 % |-+..................:....................:....................:...................:....................:..$$$$$...........+-|
//	     |                    :                    :                    :                   :                    :       $$$$$$       |
//	     |                    :                    :                    :                   :                    :             $$$$$  |
//	     |                    :                    :                    :                   :                    :                  $$|
//	     |                    :                    :                    :                   :                    :                    |
//	     |                    +                    +                    +                   +                    +                    |
//	20 % +----------------------------------------------------------------------------------------------------------------------------+
//	  enwik3               enwik4               enwik5               enwik6              enwik7               enwik8               enwik9
//
// [Zip bomb]: https://en.wikipedia.org/wiki/Zip_bomb
package main // import "modernc.org/ace/ace"

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"math/bits"
	"os"
	"path/filepath"
	"slices"
	"strings"

	"github.com/influxdata/influxdb/v2/pkg/mmap"
)

const (
	eof       = 256
	ext       = ".ace"
	firstMeta = eof + 1
	window    = 32
)

var (
	oW = flag.Bool("w", false, "overwrite existing output file")
)

func fail(rc int, msg string, args ...any) {
	s := fmt.Sprintf(msg, args...)
	fmt.Fprintln(os.Stderr, strings.TrimSpace(s))
	os.Exit(rc)
}

func main() {
	flag.Parse()

	defer func() {
		if err := recover(); err != nil {
			fail(1, "%s", err)
		}
	}()

	switch flag.NArg() {
	case 1:
		fn := flag.Arg(0)
		switch {
		case strings.HasSuffix(fn, ".ace"):
			if _, _, err := decode(fn, fn[:len(fn)-len(ext)]); err != nil {
				fail(1, "%s", err)
			}
		default:
			if _, _, err := encode(fn, fn+ext); err != nil {
				fail(1, "%s", err)
			}
		}
	case 2:
		ifn := filepath.Clean(flag.Arg(0))
		ofn := filepath.Clean(flag.Arg(1))
		if ifn == ofn {
			fail(2, "input and output files are the same")
		}

		switch {
		case strings.HasSuffix(ifn, ext) && strings.HasSuffix(ofn, ext):
			fail(2, "both file arguments have the archive extension")
		case strings.HasSuffix(ifn, ext):
			if _, _, err := decode(ifn, ofn); err != nil {
				fail(1, "%s", err)
			}
		case strings.HasSuffix(ofn, ext):
			if _, _, err := encode(ifn, ofn); err != nil {
				fail(1, "%s", err)
			}
		default:
			fail(2, "no file argument has the archive extension: %s", ext)
		}
	default:
		fail(2, "expected 1 or 2 file arguments")
	}
}

// https://oeis.org/A083652
//
// Sum of lengths of binary expansions of 0 through n.
//
// 1, 2, 4, 6, 9, 12, 15, 18, 22, 26, 30, 34, 38, 42, 46, 50, 55, 60, 65, 70, 75, 80, 85, 90, ...
//
// a(n) = 2 + (n+1)*ceiling(log_2(n+1)) - 2^ceiling(log_2(n+1)).
//
// Accounts for the extra 8 bits. n >= 2
func bitSum(n uint64) uint64 {
	n += 255
	ceil := uint64(bits.Len64(n + 1))
	return (n+1)*(ceil) - 1<<ceil - 1794
}

// Accounts for the extra 8 bits.
func bitWidth(n uint32) int {
	return bits.Len32(n + 256)
}

// symbol represents a sequence item.
type symbol uint32

type pair struct{ a, b symbol }

// encoder represents the encoding state. encoder can handle max ~4GB inputs.
type encoder struct {
	bufBits     int
	in          int64
	out         int64
	pair2Pos    map[pair]uint32
	pos         uint32
	prev        symbol
	reduceIndex int
	w           *bufio.Writer
	window      []symbol

	bitBuf byte
}

// newEncoder returns a newly created Encoder.
func newEncoder(w *bufio.Writer) *encoder {
	return &encoder{
		w:        w,
		pair2Pos: map[pair]uint32{},
		window:   make([]symbol, 0, window),
	}
}

// encode adds the encoding of 's' to the encoded sequence.
func (e *encoder) encode(s byte) error {
	e.in++
	if len(e.window) == cap(e.window) {
		if err := e.reduce(); err != nil {
			return err
		}
	}
	e.window = append(e.window, symbol(s))
	return nil
}

func (e *encoder) reduce() error {
	for i := e.reduceIndex; i < len(e.window)-1; i++ {
		p := pair{e.window[i], e.window[i+1]}
		if pos, ok := e.pair2Pos[p]; ok {
			e.window[i] = symbol(pos) + firstMeta
			e.window = slices.Delete(e.window, i+1, i+2)
			e.reduceIndex = max(0, i-1)
			return nil
		}
	}

	s := e.window[0]
	e.window = slices.Delete(e.window, 0, 1)
	e.reduceIndex = 0
	if err := e.write(s); err != nil {
		return err
	}

	if e.pos != 0 {
		p := pair{e.prev, s}
		if _, ok := e.pair2Pos[p]; !ok {
			e.pair2Pos[p] = e.pos - 1
		}
	}
	if e.pos+firstMeta == 0 {
		return errors.New("encoder input exceeds limits")
	}

	e.pos++
	e.prev = s
	return nil
}

func (e *encoder) write(s symbol) error {
	bits := 8
	if e.pos > 1 {
		bits = bitWidth(uint32(e.pos))
	}
	mask := symbol(1) << (bits - 1)
	for i := 0; i < bits; i++ {
		switch s & mask {
		case 0:
			if err := e.writeBit(0); err != nil {
				return err
			}
		default:
			if err := e.writeBit(1); err != nil {
				return err
			}
		}
		mask >>= 1
	}
	return nil
}

func (e *encoder) writeBit(b byte) error {
	if e.bufBits == 8 {
		if err := e.flushByte(); err != nil {
			return err
		}
	}

	e.bitBuf = e.bitBuf<<1 | b
	e.bufBits++
	return nil
}

func (e *encoder) flushByte() error {
	if err := e.w.WriteByte(e.bitBuf); err != nil {
		return err
	}

	e.out++
	e.bitBuf = 0
	e.bufBits = 0
	return nil
}

func (e *encoder) flush() error {
	for len(e.window) != 0 {
		if err := e.reduce(); err != nil {
			return err
		}
	}
	if e.in > 2 {
		if err := e.write(eof); err != nil {
			return err
		}
	}
	return e.flushBits()
}

func (e *encoder) flushBits() error {
	if e.bufBits == 0 {
		return nil
	}

	for e.bufBits%8 != 0 {
		if err := e.writeBit(0); err != nil {
			return err
		}
	}
	return e.flushByte()
}

func encode(ifn, ofn string) (in, out int64, err error) {
	if _, err := os.Stat(ofn); err == nil && !*oW {
		return 0, 0, fmt.Errorf("output file exists: %s", ofn)
	}

	f, err := os.Open(ifn)
	if err != nil {
		return 0, 0, fmt.Errorf("opening %s: %v", ifn, err)
	}

	defer f.Close()

	r := bufio.NewReader(f)
	g, err := os.Create(ofn)
	if err != nil {
		return 0, 0, fmt.Errorf("creating %s: %v", ofn, err)
	}

	defer func() {
		if e := g.Close(); e != nil {
			err = errors.Join(err, e)
		}
	}()

	w := bufio.NewWriter(g)

	defer func() {
		if e := w.Flush(); e != nil {
			err = errors.Join(err, e)
		}
	}()

	e := newEncoder(w)
	for {
		b, err := r.ReadByte()
		if err != nil {
			if err != io.EOF {
				return e.in, e.out, fmt.Errorf("reading %s: %v", ifn, err)
			}

			return e.in, e.out, e.flush()
		}

		if err := e.encode(b); err != nil {
			return e.in, e.out, err
		}
	}
}

type decoder struct {
	grammar []byte
	in      int64
	out     int64
	w       *bufio.Writer
}

func newDecoder(w *bufio.Writer, grammar []byte) *decoder {
	return &decoder{grammar: grammar, w: w}
}

func (d *decoder) read(pos uint64) (r symbol) {
	switch pos {
	case 0, 1:
		return symbol(d.grammar[pos])
	}

	bitOff := bitSum(pos)
	bits := bitWidth(uint32(pos))
	mask := byte(0x80) >> (bitOff & 7)
	off := bitOff >> 3
	for i := 0; i < bits; i++ {
		r <<= 1
		if d.grammar[off]&mask != 0 {
			r |= 1
		}
		switch mask {
		case 1:
			mask = 0x80
			off++
		default:
			mask >>= 1
		}
	}
	return r
}

func (d *decoder) decode(s symbol) error {
	switch {
	case s >= firstMeta:
		s -= firstMeta
		d.decode(d.read(uint64(s)))
		d.decode(d.read(uint64(s) + 1))
		return nil
	default:
		d.out++
		return d.w.WriteByte(byte(s))
	}
}

func decode(ifn, ofn string) (in, out int64, err error) {
	if _, err := os.Stat(ofn); err == nil && !*oW {
		return 0, 0, fmt.Errorf("output file exists: %s", ofn)
	}

	g, err := os.Create(ofn)
	if err != nil {
		return 0, 0, fmt.Errorf("creating %s: %v", ofn, err)
	}

	defer func() {
		if e := g.Close(); e != nil {
			err = errors.Join(err, e)
		}
	}()

	fi, err := os.Stat(ifn)
	if err != nil {
		return 0, 0, err
	}

	sz := fi.Size()
	m, err := mmap.Map(ifn, sz)
	if err != nil {
		return 0, 0, err
	}

	defer func() {
		if e := mmap.Unmap(m); e != nil {
			err = errors.Join(err, e)
		}
	}()

	if sz < 3 {
		if _, err = g.Write(m[:sz]); err != nil {
			err = fmt.Errorf("writing %s: %v", ofn, err)
		}
		return sz, sz, err
	}

	w := bufio.NewWriter(g)

	defer func() {
		if e := w.Flush(); e != nil {
			err = errors.Join(err, e)
		}
	}()

	d := newDecoder(w, m)
	for {
		s := d.read(uint64(d.in))
		if s == eof {
			return d.in, d.out, nil
		}

		d.decode(s)
		d.in++
	}
}
