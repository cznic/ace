module modernc.org/ace

go 1.22.2

require (
	github.com/dustin/go-humanize v1.0.1
	github.com/influxdata/influxdb/v2 v2.7.6
	modernc.org/plot v1.0.2
)

require (
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	modernc.org/golex v1.0.0 // indirect
	modernc.org/mathutil v1.2.1 // indirect
	modernc.org/strutil v1.0.0 // indirect
	modernc.org/xc v1.0.0 // indirect
)
