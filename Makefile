# Copyright 2024 Jan Mercl. All rights reserved.
# Use of the source code is governed by a BSD-style
# license that can be found in the LICENSE file.

.PHONY:	clean edit editor test benchmark benchstat

benchmark:
	go test -run @ -bench . 2>&1 | tee log-benchmark

benchstat:
	go test -run @ -bench=. -count=10 2>&1 | tee log-benchstat

clean:
	rm -f log-* cpu.test mem.test *.out
	go clean

edit:
	@touch log
	@if [ -f "Session.vim" ]; then gvim -S & else gvim -p Makefile go.mod all_test.go ace.go & fi

editor:
	echo -n > log-editor
	gofmt -l -s -w *.go 2>&1 | tee -a log-editor
	go test -o /dev/null -c 2>&1 | tee -a log-editor
	go install -v  2>&1 | tee -a log-editor
	staticcheck

test:
	go test -v -timeout 24h 2>&1 | tee log-test
